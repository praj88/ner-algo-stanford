FROM ujwal19/alpine-py3-java8

# File Author / Maintainer
MAINTAINER Prajwal Shreyas

# install sdk for alpine linux
RUN apk add --update --no-cache \
    python3-dev \
    build-base

#Install jupyter kernel gateway
#RUN pip install jupyter_kernel_gateway

#Install Curl
RUN apk add --update curl && \
    rm -rf /var/cache/apk/*

# Add the app file
ADD /app-ner-algo /app-ner-algo

# Copy the Stanford NER folder inside the container
#ADD /stanford-ner-2015-01-30 /stanford-ner-2015-01-30


# Get pip to download and install requirements:
RUN mv /app-ner-algo/nltk_data /usr/share
RUN pip install -r /app-ner-algo/requirements.txt


# Expose ports
EXPOSE 5007


# Set the default directory where CMD will execute
WORKDIR /app-ner-algo
CMD python3 /app-ner-algo/ner.py
