
#from pycorenlp import StanfordCoreNLP
import pandas as pd
# import subprocess
# from subprocess import Popen, PIPE
# import sh
# import threading
# from threading import Thread
#from string import punctuation
import nltk
from nltk.tag import StanfordNERTagger
import numpy as np
from flask import Flask, url_for, request
import json
 # Python 2/3 compatibility
import numpy as np
import re
import json
import decimal
import pytz
from pytz import timezone
import time
import datetime
from datetime import timedelta
import os
import uuid

from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer # load nltk's SnowballStemmer as variabled 'stemmer'
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer


DEBUG = False

# Path to NER Models used, change as per the local paths
# /Users/prajwalshreyas/Desktop/Singularity/dockerApps/tweet-analysis-algos

java_path = "/usr/lib/jvm/java-8-oracle/bin/java" # replace this
os.environ['JAVAHOME'] = java_path

#Test NER API
app = Flask(__name__)

@app.route('/nertest')
def nertest():
    #count = redis.incr('hits')
    return 'NER API working'

@app.route('/ner', methods=['POST'])
def ner():

    if request.method == 'POST':
        text_data = pd.DataFrame(request.json)
        print ('Input')
        #text_data = pd.read_json('sample_text.json')
        text_out = get_NER(text_data)
        text_out = text_out[['ref','Person','Place','Organization']]
        print ('Output')
        #Convert df t dict and the to Json
        text_out_dict = text_out.to_dict(orient='records')
        text_out_json = json.dumps(text_out_dict, ensure_ascii=False)
        print ('OUT JSON')

        return text_out_json

def get_NER(text_data):
    #/Users/prajwalshreyas/Desktop/Singularity/dockerApps/ner-algo/stanford-ner-2015-01-30
    stanford_classifier = 'stanford-ner-2015-01-30/classifiers/english.all.3class.distsim.crf.ser.gz'
    stanford_ner_path = 'stanford-ner-2015-01-30/stanford-ner.jar'

    #try:
        # Creating Tagger Object
    st = StanfordNERTagger(stanford_classifier, stanford_ner_path, encoding='utf-8')
    #except Exception as e:
    #       print (e)

    # Get keyword for the input data frame
    #keyword = tweetDataFrame.keyword.unique()
    # Subset column containing tweet text and convert to list
    # next insert a placeholder ' 00001NER ' to signify end of individual tweets

    #text_data = pd.read_json('/Users/prajwalshreyas/Desktop/Singularity/dockerApps/sentiment-algo/app-sentiment-algo/sample_text.json')
    print ('start get_NER')
    text_out = text_data.copy()
    doc = [ docs + ' 00001NER ' for docs in list(text_data['text'])]
    # ------------------------- Stanford Named Entity Recognition
    tokens = nltk.word_tokenize(str(doc))
    entities = st.tag(tokens) # actual tagging takes place using Stanford NER algorithm


    entities = [list(elem) for elem in entities] # Convert list of tuples to list of list
    print ('tag complete')
    for idx,element in enumerate(entities):
        try:
            if entities[idx][0] == '00001NER':
                entities[idx][1] = "DOC_NUMBER"  #  Modify data by adding the tag "Doc_Number"
            #elif entities[idx][0].lower() == keyword:
            #    entities[idx][1] = "KEYWORD"
            # Combine First and Last name into a single word
            elif entities[idx][1] == "PERSON" and entities[idx + 1][1] == "PERSON":
                entities[idx + 1][0] = entities[idx][0] + '-' + entities[idx+1][0]
                entities[idx][1] = 'Combined'
            # Combine consecutive Organization names
            elif entities[idx][1] == 'ORGANIZATION' and entities[idx + 1][1] == 'ORGANIZATION':
                entities[idx + 1][0] = entities[idx][0] + '-' + entities[idx+1][0]
                entities[idx][1] = 'Combined'
        except IndexError:
            break
    print ('enumerate complete')
    # Filter list of list for the words we are interested in
    filter_list = ['DOC_NUMBER','PERSON','LOCATION','ORGANIZATION']
    entityWordList = [element for element in entities if any(i in element for i in filter_list)]

    entityString = ' '.join(str(word) for insideList in entityWordList for word in insideList) # convert list to string and concatenate it
    entitySubString = entityString.split("DOC_NUMBER") # split the string using the separator 'TWEET_NUMBER'
    del entitySubString[-1] # delete the extra blank row created in the previous step

    # Store the classified NERs in the main tweet data frame
    for idx,docNER in enumerate(entitySubString):
        docNER = docNER.strip().split() # split the string into word list
        # Filter for words tagged as Organization and store it in data frame
        text_out.loc[idx,'Organization'] =  ','.join([docNER[i-1]  for i,x in enumerate(docNER) if x == 'ORGANIZATION'])
        # Filter for words tagged as LOCATION and store it in data frame
        text_out.loc[idx,'Place'] = ','.join([docNER[i-1] for i,x in enumerate(docNER) if x == 'LOCATION'])
        # Filter for words tagged as PERSON and store it in data frame
        text_out.loc[idx,'Person'] = ','.join([docNER[i-1]  for i,x in enumerate(docNER) if x == 'PERSON'])

    print ('process complete')
    return text_out


if __name__ == "__main__":
     app.run(host="0.0.0.0", debug=False, port=5007)
